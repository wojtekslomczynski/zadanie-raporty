import { from, Observable } from "../node_modules/rxjs";
import { map, concatMap, filter } from "../node_modules/rxjs/operators";

interface Report {
  category: string;
  date: number;
  description: string;
  files: { filename: string; filesize: number }[];
  title: string;
}

const data_url = "./data.json";
const reportsData = fetchReports(data_url);
const selectedYearListener = document
  .getElementById("years-dropdown")
  .addEventListener("change", renderReports);
const searchBoxListener = document
  .getElementById("search-box")
  .addEventListener("change", renderReports);
const searchButtonListener = document
  .getElementById("search-button")
  .addEventListener("change", renderReports);
const toggleAllButtonListener = document
  .getElementById("toggle-all-button")
  .addEventListener("change", toggleAllCategories);

function fetchReports(data_url: string): Observable<Report[]> {
  return new Observable((observer) => {
    fetch(data_url)
      .then((response) => response.json())
      .then((body) => {
        observer.next(body);
        observer.complete();
      });
  });
}

(function initReportList() {
  const reportYears: number[] = [];
  const reportCategories: string[] = [];

  reportsData
    .pipe(
      concatMap((list) => from(list)),
      map(({ date, category }) => ({
        year: new Date(date).getFullYear(),
        category: category,
      }))
    )
    .subscribe({
      next: ({ year, category }) => {
        reportYears.push(year);
        reportCategories.push(category);
      },
      complete: () => {
        const yearsDropdown = document.getElementById("years-dropdown");
        reportYears
          .filter((value, index, array) => array.indexOf(value) === index)
          .sort((a, b) => b - a)
          .forEach((year) => {
            const yearOption = document.createElement("option");
            yearOption.innerText = `${year}`;
            yearsDropdown.appendChild(yearOption);
          });

        const categoryList = document.getElementById("category-list");
        reportCategories
          .filter((value, index, array) => array.indexOf(value) === index)
          .sort()
          .forEach((category) => {
            const categoryInput = document.createElement("input");
            categoryInput.classList.add("btn-check", "category");
            categoryInput.setAttribute("type", "checkbox");
            categoryInput.setAttribute("id", category);
            categoryList.appendChild(categoryInput);
            categoryInput.addEventListener("change", handleCategoryToggle);

            const categoryLabel = document.createElement("label");
            categoryLabel.classList.add("btn", "btn-outline-primary");
            categoryLabel.setAttribute("for", category);
            categoryLabel.innerText = `${category}`;
            categoryList.appendChild(categoryLabel);
          });

        renderReports();
      },
    });
})();

function renderReports() {
  let reports: Report[] = [];
  const selectedYear = getYearSelection();
  const searchedPhrase = getPhraseInput();
  const selectedCategories = getCategoriesSelection();

  reportsData
    .pipe(
      concatMap((list) => from(list)),
      filter(({ date }) => new Date(date).getFullYear() === selectedYear),
      filter(({ title }) => title.toLowerCase().includes(searchedPhrase)),
      filter(
        ({ category }) =>
          selectedCategories.includes(category) ||
          selectedCategories.length === 0
      )
    )
    .subscribe({
      next: (report) => reports.push(report),
      complete: () => renderFilteredReports(reports),
    });
}

function renderFilteredReports(reports: Report[]) {
  const reportList = document.getElementById("report-list");

  while (reportList.firstChild) {
    reportList.firstChild.remove();
  }

  reports.forEach((report, index) =>
    reportList.appendChild(renderReport(report, index))
  );
}

function renderReport(report: Report, i: number) {
  const listItem = document.createElement("li");
  listItem.classList.add("list-group-item", "report", "p-4");

  const row = document.createElement("div");
  row.classList.add("row");

  const leftCol = document.createElement("div");
  leftCol.classList.add("col-2");

  const date = document.createElement("b");
  date.classList.add("lh-lg");
  date.innerText = new Date(report.date).toLocaleDateString("pl");
  leftCol.appendChild(date);

  const time = document.createElement("b");
  time.innerText = new Date(report.date).toLocaleTimeString("pl");
  time.classList.add("d-block");
  leftCol.appendChild(time);

  const category = document.createElement("p");
  category.innerText = report.category;
  category.classList.add("mt-2");
  leftCol.appendChild(category);

  const rightCol = document.createElement("div");
  rightCol.classList.add("col");

  const title = document.createElement("h2");
  title.innerText = report.title.slice(0, 50);
  rightCol.appendChild(title);

  const description = document.createElement("h5");
  description.innerText = report.description;
  rightCol.appendChild(description);

  const anchor = document.createElement("a");
  anchor.classList.add("text-decoration-none");
  anchor.setAttribute("href", "#");
  anchor.innerText = "Zobacz raport";

  const fileCol = document.createElement("div");
  fileCol.classList.add("col-3");
  fileCol.appendChild(anchor);

  const filesCol = document.createElement("div");
  filesCol.classList.add("col");

  if (report.files.length === 1) {
    const singleFile = document.createElement("a");
    singleFile.classList.add("text-decoration-none", "d-block");
    singleFile.setAttribute("href", "#");
    singleFile.innerText = `${report.files[0].filename}.pdf (${report.files[0].filesize}kB)`;
    filesCol.appendChild(singleFile);
  } else if (report.files.length > 1) {
    const accordion = document.createElement("div");
    accordion.setAttribute("id", `accordion-flush-${i}`);
    accordion.classList.add("accordion", "accordion-flush");

    const accordionItem = document.createElement("div");
    accordionItem.classList.add("accordion-item");

    const accordionHeader = document.createElement("p");
    accordionHeader.setAttribute("id", `accordion-header-${i}`);
    accordionHeader.classList.add("accordion-header");

    const accordionButton = document.createElement("button");
    accordionButton.setAttribute("type", "button");
    accordionButton.setAttribute("data-bs-toggle", "collapse");
    accordionButton.setAttribute("data-bs-target", `#accordion-collapse-${i}`);

    accordionButton.setAttribute("aria-expanded", "false");
    accordionButton.setAttribute("aria-controls", "accordion-collapse");
    accordionButton.classList.add(
      "accordion-button",
      "collapsed",
      "p-0",
      "bg-transparent",
      "text-primary"
    );
    accordionButton.innerText = `Pliki do pobrania (${report.files.length})`;
    accordionHeader.appendChild(accordionButton);

    const accordionCollapse = document.createElement("div");
    accordionCollapse.setAttribute("id", `accordion-collapse-${i}`);
    accordionCollapse.setAttribute("aria-labelledby", `accordion-header-${i}`);

    accordionCollapse.setAttribute("data-bs-parent", `#accordion-flush-${i}`);
    accordionCollapse.classList.add("accordion-collapse", "collapse");

    const accordionBody = document.createElement("div");
    accordionBody.classList.add("accordion-body", "p-0");

    report.files.forEach((file) => {
      const singleFile = document.createElement("a");
      singleFile.classList.add("text-decoration-none", "d-block", "my-2");
      singleFile.setAttribute("href", "#");
      singleFile.innerText = `${file.filename}.pdf (${file.filesize}kB)`;
      accordionBody.appendChild(singleFile);
    });

    accordionCollapse.appendChild(accordionBody);
    accordionItem.appendChild(accordionHeader);
    accordionItem.appendChild(accordionCollapse);
    accordion.appendChild(accordionItem);
    filesCol.appendChild(accordion);
  }

  const fileRow = document.createElement("div");
  fileRow.classList.add("row");
  fileRow.appendChild(fileCol);
  fileRow.appendChild(filesCol);

  rightCol.appendChild(fileRow);

  row.appendChild(leftCol);
  row.appendChild(rightCol);

  listItem.appendChild(row);

  return listItem;
}

function getYearSelection(): number {
  const select = document.getElementById("years-dropdown") as HTMLSelectElement;

  return parseInt(select.value);
}

function getPhraseInput(): string {
  const searchBox = document.getElementById("search-box") as HTMLInputElement;
  const searchedPhrase = searchBox.value.trim().toLowerCase();

  return searchedPhrase;
}

function getCategoriesSelection(): string[] {
  const categories = [
    ...document.getElementsByClassName("category"),
  ] as HTMLInputElement[];

  return categories
    .filter(({ checked }) => checked === true)
    .map(({ id }) => id);
}

function handleCategoryToggle() {
  renderReports();

  const categories = [
    ...document.getElementsByClassName("category"),
  ] as HTMLInputElement[];
  const toggleAllButton = document.getElementById(
    "toggle-all-button"
  ) as HTMLInputElement;

  toggleAllButton.checked =
    categories.findIndex(({ checked }) => !checked) === -1;
}

function toggleAllCategories() {
  renderReports();

  const toggleAllButton = document.getElementById(
    "toggle-all-button"
  ) as HTMLInputElement;
  const toggleState = toggleAllButton.checked;
  const categories = [
    ...document.getElementsByClassName("category"),
  ] as HTMLInputElement[];

  categories.forEach(({ checked }) => (checked = toggleState));
}
